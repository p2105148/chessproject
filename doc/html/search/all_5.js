var searchData=
[
  ['game_0',['Game',['../class_game.html',1,'Game'],['../class_game.html#ad59df6562a58a614fda24622d3715b65',1,'Game::Game()']]],
  ['gametxt_1',['gameTxt',['../classgame_txt.html',1,'gameTxt'],['../classgame_txt.html#ad5ee454e2f892e189248a00fbab343d4',1,'gameTxt::gameTxt()']]],
  ['getcurrentturn_2',['getCurrentTurn',['../class_game.html#ae182d917809114f43da68294741a0844',1,'Game']]],
  ['getlastmove_3',['getLastMove',['../class_game.html#a9ad7d4d6873fd7dbb172945f5ef42097',1,'Game']]],
  ['getopponentcolor_4',['getOpponentColor',['../class_game.html#aaceec8bb090a840162683c47e5fdfa7b',1,'Game']]],
  ['getpiece_5fconsidermove_5',['getPiece_considerMove',['../class_game.html#a9d3602e32d9c6eeb7b73e05faaeea136',1,'Game']]],
  ['getpieceatposition_6',['getPieceAtPosition',['../class_game.html#a8aaa9dd2a4ab7c9c853658631ef75788',1,'Game::getPieceAtPosition(Position pos)'],['../class_game.html#a8948766a69ab8a539a7866f06fec8e47',1,'Game::getPieceAtPosition(int iRow, int iColumn)']]],
  ['getpiececolor_7',['getPieceColor',['../class_chess.html#aff2b08644cfed30664ca91cef92e4ae0',1,'Chess']]],
  ['gui_8',['GUI',['../class_g_u_i.html',1,'']]]
];
