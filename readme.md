KIEU Minh Dung - p2205374
PHAM Chien Cong - p2105148
Identifiant sur la forge: 33551

Jeu d'échecs normale - projet LIFAPCD
Un petit attention: Nous avons un problème avec SDL, et nous avons pas du temps donc nous avons seulement le mainTxt dans le bin.

Commande de compilation: Il faut télécharger cmake et aller dans le build répertoire et saisir "cmake ..", ensuite "make". Un fichier éxécutable va afficher dans le répertoire bin. Exécuter par la commande "./bin/mainTxt"

La programme a tous les fonctionnalité necessaire: roque, promotion du pion, checkmate, check, en passant,...

Organisation de l'archive:

bin : répertoire pour les éxécutables : mainTxt est le jeu en  mode texte

build : répertoire pour le makefile : tous les fichiers généré par CMakeLists.txt sont ici.

CMakeLists.txt: le fichier pour construire le projet

data : répertoire pour les assets

doc: répertoire pour la documentation, diagramme de UML, diagramme de Gantt, le code généré par Doxygen

src: répertoire pour tous les sources code
