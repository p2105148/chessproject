#ifndef _GAME_H_
#define _GAME_H_

#include "includes.h"
#include "Chess.h"

/**
 * @brief The Game class represents a chess game.
 */
class Game : public Chess
{
public:
    /**
     * @brief Constructs a Game object.
     */
    Game();

    /**
     * @brief Destroys the Game object.
     */
    ~Game();

    /**
     * @brief Moves a chess piece from the present position to the future position.
     * @param present The present position of the piece.
     * @param future The future position of the piece.
     * @param S_enPassant Pointer to the EnPassant object.
     * @param S_castling Pointer to the Castling object.
     * @param S_promotion Pointer to the Promotion object.
     */
    void movePiece(Position present, Position future, EnPassant* S_enPassant, Castling* S_castling, Promotion* S_promotion);

    /**
     * @brief Checks if castling is allowed for the specified side and color.
     * @param iSide The side (WHITE or BLACK).
     * @param iColor The color (WHITE or BLACK).
     * @return True if castling is allowed, false otherwise.
     */
    bool castlingAllowed(Side iSide, int iColor);

    /**
     * @brief Gets the piece at the specified position.
     * @param pos The position.
     * @return The piece at the position.
     */
    char getPieceAtPosition(Position pos);

    /**
     * @brief Gets the piece at the specified row and column.
     * @param iRow The row.
     * @param iColumn The column.
     * @return The piece at the specified row and column.
     */
    char getPieceAtPosition(int iRow, int iColumn);

    /**
     * @brief Gets the piece at the specified row and column, considering a potential move.
     * @param iRow The row.
     * @param iColumn The column.
     * @param intended_move Pointer to the IntendedMove object.
     * @return The piece at the specified row and column, considering the intended move.
     */
    char getPiece_considerMove(int iRow, int iColumn, IntendedMove* intended_move = nullptr);

    /**
     * @brief Checks if the specified position is under attack by the specified color.
     * @param iRow The row.
     * @param iColumn The column.
     * @param iColor The color (WHITE or BLACK).
     * @param pintended_move Pointer to the IntendedMove object.
     * @return The UnderAttack value indicating the attack status.
     */
    UnderAttack isUnderAttack(int iRow, int iColumn, int iColor, IntendedMove* pintended_move = nullptr);

    /**
     * @brief Checks if the specified position is reachable by the specified color.
     * @param iRow The row.
     * @param iColumn The column.
     * @param iColor The color (WHITE or BLACK).
     * @return True if the position is reachable, false otherwise.
     */
    bool isReachable(int iRow, int iColumn, int iColor);

    /**
     * @brief Checks if the specified square is occupied.
     * @param iRow The row.
     * @param iColumn The column.
     * @return True if the square is occupied, false otherwise.
     */
    bool isSquareOccupied(int iRow, int iColumn);

    /**
     * @brief Checks if the path between two positions is free of obstacles.
     * @param startingPos The starting position.
     * @param finishingPos The finishing position.
     * @param iDirection The direction of movement.
     * @return True if the path is free, false otherwise.
     */
    bool isPathFree(Position startingPos, Position finishingPos, int iDirection);

    /**
     * @brief Checks if a piece can be blocked between two positions.
     * @param startingPos The starting position.
     * @param finishinPos The finishing position.
     * @param iDirection The direction of movement.
     * @return True if the piece can be blocked, false otherwise.
     */
    bool canBeBlocked(Position startingPos, Position finishinPos, int iDirection);

    /**
     * @brief Checks if the game is in a checkmate state.
     * @return True if the game is in checkmate, false otherwise.
     */
    bool isCheckMate();

    /**
     * @brief Checks if the king of the specified color is in check.
     * @param iColor The color (WHITE or BLACK).
     * @param intended_move Pointer to the IntendedMove object.
     * @return True if the king is in check, false otherwise.
     */
    bool isKingInCheck(int iColor, IntendedMove* intended_move = nullptr);

    /**
     * @brief Checks if the player's king is in check.
     * @param intended_move Pointer to the IntendedMove object.
     * @return True if the player's king is in check, false otherwise.
     */
    bool playerKingInCheck(IntendedMove* intended_move = nullptr);

    /**
     * @brief Checks if the specified move would put the king in check.
     * @param chPiece The piece to be moved.
     * @param present The present position of the piece.
     * @param future The future position of the piece.
     * @param S_enPassant Pointer to the EnPassant object.
     * @return True if the move would put the king in check, false otherwise.
     */
    bool wouldKingBeInCheck(char chPiece, Position present, Position future, EnPassant* S_enPassant);

    /**
     * @brief Finds the position of the king of the specified color.
     * @param iColor The color (WHITE or BLACK).
     * @return The position of the king.
     */
    Position findKing(int iColor);

    /**
     * @brief Changes the turns between players.
     */
    void changeTurns();

    /**
     * @brief Checks if the game has finished.
     * @return True if the game has finished, false otherwise.
     */
    bool isFinished();

    /**
     * @brief Gets the current turn.
     * @return The current turn.
     */
    int getCurrentTurn();

    /**
     * @brief Gets the opponent's color.
     * @return The opponent's color.
     */
    int getOpponentColor();

    /**
     * @brief Parses the move string and extracts the from and to positions, and the promoted piece.
     * @param move The move string.
     * @param pFrom Pointer to the Position object to store the from position.
     * @param pTo Pointer to the Position object to store the to position.
     * @param chPromoted Pointer to the char variable to store the promoted piece.
     */
    void parseMove(string move, Position* pFrom, Position* pTo, char* chPromoted = nullptr);

    /**
     * @brief Logs the move to the record.
     * @param to_record The string to record the move.
     */
    void logMove(string& to_record);

    /**
     * @brief Gets the last move.
     * @return The last move.
     */
    string getLastMove();

    /**
     * @brief Deletes the last move from the record.
     */
    void deleteLastMove();

    // Save the captured pieces
    vector<char> white_captured;
    vector<char> black_captured;

    /**
     * @brief Represents a round of moves.
     */
    struct Round
    {
        string white_move;
        string black_move;
    };

    deque<Round> rounds;

private:
    char board[8][8];
    bool m_bCastlingKingSideAllowed[2];
    bool m_bCastlingQueenSideAllowed[2];
    int m_CurrentTurn;
    bool m_bGameFinished;
};

#endif