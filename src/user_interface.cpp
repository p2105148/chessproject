#include "user_interface.h"
#include "Game.h"
void clearScreen(void)
{
    system("cls");
}

void printMenu(void)
{
    cout << "Commands: (N)ew game, (M)ove, (Q)uit" << endl;
}

void printSituation(Game& game)
{
    // Last moves - print only if at least one move has been made
    if (game.rounds.size() != 0)
    {
        cout << "Last moves: " << endl;
        int iMoves = game.rounds.size();
        int iToShow = iMoves >= 5 ? 5 : iMoves;
        string space = "";

        while(iToShow--)
        {
            if (iMoves < 10)
            {
                space = " ";
            }
            cout << space << iMoves << " ..... " << game.rounds[iMoves-1].white_move.c_str() << " | " << game.rounds[iMoves-1].black_move.c_str() << endl;
            iMoves--;
        }
        cout << endl;
    }

    if (game.white_captured.size() != 0 || game.black_captured.size() != 0)
    {
        cout << "------------------------" << endl;
        cout <<"White captured: ";
        for (unsigned int i=0;i<game.white_captured.size();i++)
        {
            cout << char(game.white_captured[i]) << " ";
        }
        cout << endl;

        cout <<"Black captured: ";
        for (unsigned int i=0;i<game.black_captured.size();i++)
        {
            cout << char(game.black_captured[i]) << " ";
        }
        cout << "------------------------" << endl;
    }
    cout <<"Current turn: " << (game.getCurrentTurn() == Chess::WHITE_PLAYER ? "White" : "Black") << endl;
}

void printBoard(Game& game)
{
    std::cout<<"\t    "<<"  A    B     C     D     E     F     G     H   " << std::endl;
	std::cout<<"\t    "<<"_____ _____ _____ _____ _____ _____ _____ _____ " << std::endl;	
	
	for (int i = 0; i < 8 ; i++) {

		std::cout << "\t   |" << "     |     |     |     |     |     |     |     |" << std::endl;
		std::cout << "\t" << i +1 << "  |";
        for (int j=0; j<8; j++)
        {
            cout << "  " << game.getPieceAtPosition(i, j) << "  |";
        }
		std::cout << std::endl;
		std::cout << "\t   |" << "_____|_____|_____|_____|_____|_____|_____|_____|";
		std::cout << std::endl;
	}

	std::cout<<std::endl;
}