#include "includes.h"
#include "Chess.h"

int Chess::getPieceColor( char chPiece )
{
    return ( isupper( chPiece ) ? WHITE_PIECE : BLACK_PIECE );
}

bool Chess::isWhitePiece( char chPiece )
{
    return ( getPieceColor( chPiece ) == WHITE_PIECE ? true : false);
}

bool Chess::isBlackPiece( char chPiece )
{
    return ( getPieceColor( chPiece ) == BLACK_PIECE ? true : false);
}

string Chess::describePiece( char chPiece )
{
    string strPiece;
    switch( chPiece )
    {
        case 'P':
            strPiece = "White Pawn";
            break;
        case 'p':
            strPiece = "Black Pawn";
            break;
        case 'N':
            strPiece = "White Knight";
            break;
        case 'n':
            strPiece = "Black Knight";
            break;
        case 'B':
            strPiece = "White Bishop";
            break;
        case 'b':
            strPiece = "Black Bishop";
            break;
        case 'R':
            strPiece = "White Rook";
            break;
        case 'r':
            strPiece = "Black Rook";
            break;
        case 'Q':
            strPiece = "White Queen";
            break;
        case 'q':
            strPiece = "Black Queen";
            break;
        case 'K':
            strPiece = "White King";
            break;
        case 'k':
            strPiece = "Black King";
            break;
        default:
            strPiece = "Empty";
            break;
    }
    return strPiece;
}

/*void Chess::testRegression()
{
    assert(getPieceColor('P') == WHITE_PIECE);
    assert(getPieceColor('p') == BLACK_PIECE);
    assert(isWhitePiece('P') == true);
    assert(isWhitePiece('p') == false);
    assert(isBlackPiece('P') == false);
    assert(isBlackPiece('p') == true);
    assert(describePiece('P') == "White Pawn");
    assert(describePiece('p') == "Black Pawn");
    assert(describePiece('N') == "White Knight");
    assert(describePiece('n') == "Black Knight");
    assert(describePiece('B') == "White Bishop");
    assert(describePiece('b') == "Black Bishop");
    assert(describePiece('R') == "White Rook");
    assert(describePiece('r') == "Black Rook");
    assert(describePiece('Q') == "White Queen");
    assert(describePiece('q') == "Black Queen");
    assert(describePiece('K') == "White King");
    assert(describePiece('k') == "Black King");
    assert(describePiece('.') == "Empty");

    cout << "All tests passed successfully!" << endl;
}*/