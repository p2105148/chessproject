#include"gameTxt.h"
#include"Chess.h"

gameTxt::gameTxt()
{
    currentGame = new Game();
}

gameTxt::~gameTxt()
{
    delete currentGame;
}

bool gameTxt::isMoveValid(Chess::Position present, Chess::Position future, Chess::EnPassant* S_enPassant, Chess::Castling* S_castling, Chess::Promotion* S_promotion)
{
    bool bValid = false;
    char chPiece = currentGame->getPieceAtPosition(present);
    //1, Is the piece allow to move in that direction?

    switch(toupper(chPiece))
    {
        case 'P':
        {
            // wants to move forward
            if (future.iColumn == present.iColumn)
            {
                // 1 move forward
                if ((Chess::isWhitePiece(chPiece) && future.iRow == present.iRow + 1) || (Chess::isBlackPiece(chPiece) && future.iRow == present.iRow - 1))
                {
                    if (EMPTY_SQUARE == currentGame->getPieceAtPosition(future))
                    {
                        bValid = true;
                    }
                }
                // 2 move forward
                else if ((Chess::isWhitePiece(chPiece) && future.iRow == present.iRow + 2) || (Chess::isBlackPiece(chPiece) && future.iRow == present.iRow - 2))
                {
                    // this is only allow if the pawn in its original position
                    if (Chess::isWhitePiece(chPiece))
                    {
                        if (EMPTY_SQUARE == currentGame->getPieceAtPosition(future.iRow-1, future.iColumn)
                            &&  EMPTY_SQUARE == currentGame->getPieceAtPosition(future) && present.iRow == 1) bValid = true;
                    }
                    else
                    {
                        if (EMPTY_SQUARE == currentGame->getPieceAtPosition(future.iRow+1, future.iColumn)
                            &&  EMPTY_SQUARE == currentGame->getPieceAtPosition(future) && present.iRow == 6) bValid = true;
                    }
                }
                else return false;
            }
            // the en passant
            else if ((Chess::isWhitePiece(chPiece) && present.iRow == 4 && future.iRow == 5 && abs(future.iColumn-present.iColumn) == 1) ||
                        (Chess::isBlackPiece(chPiece) && present.iRow == 3 && future.iRow == 2 && abs(future.iColumn-present.iColumn) == 1))
            {
                //it's only valid if last move of the opponent was a pawn moving 2 squares forward
                string last_move = currentGame->getLastMove();
                //parse the line
                Chess::Position LastMoveFrom, LastMoveTo;
                currentGame -> parseMove(last_move, &LastMoveFrom, &LastMoveTo);

                // First of all, it's a pawn?
                char chLstMvPiece = currentGame->getPieceAtPosition(LastMoveTo);
                if (toupper(chLstMvPiece) != 'P') return false;

                // it moved 2 squares forward? and was it an adjacent column?
                if (abs(LastMoveTo.iRow - LastMoveFrom.iRow) == 2 && abs(LastMoveFrom.iColumn - present.iColumn))
                {
                    cout << "En passant" << endl;
                    bValid = true;
                    S_enPassant->bApplied = true;
                    S_enPassant->PawnCaptured.iRow = LastMoveTo.iRow;
                    S_enPassant->PawnCaptured.iColumn = LastMoveTo.iColumn;
                }
            }
            // wants to capture
            else if (abs(future.iColumn - present.iColumn) == 1)
            {
                if ((Chess::isWhitePiece(chPiece) && future.iRow == present.iRow + 1) || (Chess::isBlackPiece(chPiece) && future.iRow == present.iRow - 1))
                {
                    //only allowed if there's something to be captured
                    if (EMPTY_SQUARE != currentGame->getPieceAtPosition(future))
                    {
                        bValid = true;
                        cout << "Pawn captured a piece"<<endl;
                    }
                }
            }
            else return false;

            //promotion
            if ((Chess::isWhitePiece(chPiece) && future.iRow == 7) || (Chess::isBlackPiece(chPiece) && future.iRow == 0))
            {
                cout << "Promotion" << endl;
                S_promotion->bApplied = true;
            }
        }
        break;

        case 'R':
        {
            //Horizontal move
            if (future.iRow == present.iRow && future.iColumn != present.iColumn)
            {
                // no pieces on the way
                if (currentGame->isPathFree(present, future, Chess::HORIZONTAL)) bValid = true;
            }
            //Vertical move
            else if (future.iColumn == present.iColumn && future.iRow != present.iRow)
            {
                // no pieces on the way
                if (currentGame->isPathFree(present, future, Chess::VERTICAL)) bValid = true;
            }
            else return false;
        }
        break;

        case 'N':
        {
            if (abs(future.iRow - present.iRow) == 2 && abs(future.iColumn - present.iColumn) == 1) bValid = true;
            else if (abs(future.iRow - present.iRow) == 1 && abs(future.iColumn - present.iColumn) == 2) bValid = true;
            else return false;
        }
        break;

        case 'B':
        {
            if (abs(future.iRow - present.iRow) == abs(future.iColumn - present.iColumn))
            {
                // no pieces on the way
                if (currentGame->isPathFree(present, future, Chess::DIAGONAL)) bValid = true;
            }
            else return false;
        }
        break;

        case 'Q':
        {
            //Horizontal move
            if (future.iRow == present.iRow && future.iColumn != present.iColumn)
            {
                // no pieces on the way
                if (currentGame->isPathFree(present, future, Chess::HORIZONTAL)) bValid = true;
            }
            //Vertical move
            else if (future.iColumn == present.iColumn && future.iRow != present.iRow)
            {
                // no pieces on the way
                if (currentGame->isPathFree(present, future, Chess::VERTICAL)) bValid = true;
            }
            //Diagonal move
            else if (abs(future.iRow - present.iRow) == abs(future.iColumn - present.iColumn))
            {
                // no pieces on the way
                if (currentGame->isPathFree(present, future, Chess::DIAGONAL)) bValid = true;
            }
            else return false;
        }
        break;

        case 'K':
        {
            // Horizontal move by 1
            if (abs(future.iRow - present.iRow) == 1 && (future.iColumn == present.iColumn))
            {
                bValid = true;
            }
            // Vertical move by 1
            else if (abs(future.iColumn - present.iColumn) == 1 && (future.iRow == present.iRow))
            {
                bValid = true;
            }
            // Diagonal move by 1
            else if (abs(future.iRow - present.iRow) == 1 && abs(future.iColumn - present.iColumn) == 1)
            {
                bValid = true;
            }
            // castling
            else if (abs(future.iColumn - present.iColumn) == 2 && future.iRow == present.iRow)
            {
                //1, King not in check
                if (currentGame->playerKingInCheck()) return false;

                //2, No pieces between the king and the rook
                if (currentGame->isPathFree(present, future, Chess::HORIZONTAL) == false) return false;

                //3, The king and the rook haven't moved
                //4, King mustn't pass through a square that is attacked by an enemy piece
                if (future.iColumn > present.iColumn)
                {
                    // if future.iColumn is greater, then it's a king side
                    if (currentGame -> castlingAllowed(Chess::Side::KING_SIDE, Chess::getPieceColor(chPiece)) == false)
                    {
                        cout << "Castling to the king side is not allowed" << endl;
                        return false;
                    }
                    else 
                    {
                        // check if the square that the king jump through is not under attack
                        Chess::UnderAttack square_skipped = currentGame->isUnderAttack(present.iRow, present.iColumn+1, currentGame->getCurrentTurn());
                        if (square_skipped.bUnderAttack == false)
                        {
                            S_castling->bApplied = true;
                            S_castling->rook_before.iRow = present.iRow;
                            S_castling->rook_before.iColumn = present.iColumn+3;

                            S_castling->rook_after.iRow = future.iRow;
                            S_castling->rook_after.iColumn = present.iColumn+1;

                            bValid = true;
                        }
                    }
                }
                else //Queen side
                {
                    //auto queenside = Chess::Side::QUEEN_SIDE;
                    
                    if (currentGame -> castlingAllowed(Chess::Side::QUEEN_SIDE, Chess::getPieceColor(chPiece)) == false)
                    {
                        cout << "Castling to the queen side is not allowed" << endl;
                        return false;
                    }
                    else 
                    {
                        // check if the square that the king jump through is not under attack
                        Chess::UnderAttack square_skipped = currentGame->isUnderAttack(present.iRow, present.iColumn-1, currentGame->getCurrentTurn());
                        if (square_skipped.bUnderAttack == false)
                        {
                            S_castling->bApplied = true;
                            S_castling->rook_before.iRow = present.iRow;
                            S_castling->rook_before.iColumn = present.iColumn-4;

                            S_castling->rook_after.iRow = future.iRow;
                            S_castling->rook_after.iColumn = present.iColumn-1;

                            bValid = true;
                        }
                    }
                }
            }
        }
        break;

        default:
            cout << "INVALID" << endl;
        break;
    }
    if (bValid == false)
        return false;
    //2, Is there another piece of the same color on the destination square?
    if (currentGame->isSquareOccupied(future.iRow, future.iColumn))
    {
        char chAuxPiece = currentGame->getPieceAtPosition(future);
        if (Chess::getPieceColor(chPiece) == Chess::getPieceColor(chAuxPiece))
        {
            cout << "Position is already occupied by a piece of the same color" << endl;
            return false;
        }
    }
    //3, Would the move put the king in check?
    if (currentGame->wouldKingBeInCheck(chPiece, present, future, S_enPassant))
    {
        cout << "King would be in check" << endl;
        return false;
    }
    return bValid;
}

void gameTxt::makeTheMove(Chess::Position present, Chess::Position future, Chess::EnPassant* S_enPassant, Chess::Castling* S_castling, Chess::Promotion* S_promotion)
{
    char chPiece = currentGame->getPieceAtPosition(present);

    if(currentGame -> isSquareOccupied(future.iRow,future.iColumn))
    {
        char chAuxPiece = currentGame->getPieceAtPosition(future);
        if (Chess::getPieceColor(chPiece) != Chess::getPieceColor(chAuxPiece))
        {
            cout << Chess::describePiece(chPiece) << " captured " << Chess::describePiece(chAuxPiece) << endl;
        }
        else
        {
            cout << "Invalid move" << endl;
        }
    }
    else if (S_enPassant->bApplied)
        cout << "Pawn captured by en passant" << endl;
    if (S_castling->bApplied)
        cout << "Castling applied" << endl;
    currentGame->movePiece(present, future, S_enPassant, S_castling, S_promotion);
}

void gameTxt::newGame()
{
    if (currentGame!=nullptr)
    {
        delete currentGame;
    }
    currentGame = new Game();
}

void gameTxt::movePiece()
{
    string to_record;
    cout << "Choose piece to be moved. (e,g: A1 ou b2): ";

    string move_from;
    getline(cin, move_from);
    if (move_from.length() > 2)
    {
        cout << "Invalid input. You should only type only 2 caracs(col and row)" << endl;
        return;
    }
    Chess::Position present;
    present.iColumn = move_from[0];
    present.iRow = move_from[1];
    present.iColumn = toupper(present.iColumn);

    if (present.iColumn < 'A' || present.iColumn >'H')
    {
        cout << "Invalid column" << endl;
        return;
    }
    if (present.iRow < '0' || present.iRow > '8')
    {
        cout << "Invalid row" << endl;
        return;
    }
    to_record += present.iColumn;
    to_record += present.iRow;
    to_record += "-";

    present.iColumn -= 'A';
    present.iRow -= '1';    

    char chPiece = currentGame -> getPieceAtPosition(present);
    cout << "Piece is " <<char(chPiece) << endl;
    if (0x20 == chPiece)
    {
        cout << "No piece in that position" << endl;
        return;
    }
    if (currentGame->getCurrentTurn() == Chess::WHITE_PIECE)
    {
        if (Chess::isWhitePiece(chPiece) == false)
        {
            cout << "It's white turn and you pick a black piece!! " << endl;
            return;
        }
    }
    else
    {
        if (Chess::isBlackPiece(chPiece) == false)
        {
            cout << "It's black turn and you pick a white piece!! " << endl;
            return;
        }
    }

    cout << "Move to?: ";
    string move_to;
    getline(cin, move_to);
    if (move_to.length() > 2)
    {
        cout << "Invalid input. You should only type only 2 caracs(col and row)" << endl;
        return;
    }
    Chess::Position future;
    future.iColumn = move_to[0];
    future.iRow = move_to[1];
    future.iColumn = toupper(future.iColumn);
    if(future.iColumn < 'A' || future.iColumn > 'H')
    {
        cout << "Invalid column" << endl;
        return;
    }
    if (future.iRow < '0' || future.iRow > '8')
    {
        cout << "Invalid row" << endl;
        return;
    }
    to_record += future.iColumn;
    to_record += future.iRow;
    future.iColumn -= 'A';
    future.iRow -= '1';
    if(future.iRow == present.iRow && future.iColumn == present.iColumn)
    {
        cout << "Invalid move, same square!!" << endl;
        return;
    }
    Chess::EnPassant S_enPassant = {0};
    Chess::Castling S_castling = {0};
    Chess::Promotion S_promotion = {0};
    if(isMoveValid(present, future, &S_enPassant, &S_castling, &S_promotion) == false)
    {
        cout << "Invalid move" << endl; 
        return;
    }
    if (S_promotion.bApplied == true)
    {
        cout << "Promote to (Q, R, B, N): ";
        string piece;
        getline(cin, piece);
        if (piece.length() > 1)
        {
            cout << "Invalid input. You should only type only 1 carac" << endl;
            return;
        }
        char chPromoted = toupper(piece[0]);
        if (chPromoted != 'Q' && chPromoted != 'R' && chPromoted != 'B' && chPromoted != 'N')
        {
            cout << "Invalid piece" << endl;
            return;
        }
        S_promotion.chBefore = currentGame->getPieceAtPosition(present);
        if(Chess::WHITE_PLAYER == currentGame->getCurrentTurn())
        {
            S_promotion.chAfter = toupper(chPromoted);
        }
        else
        {
            S_promotion.chAfter = tolower(chPromoted);
        }
        to_record += '=';
        to_record += toupper(chPromoted);
    }
    makeTheMove(present, future, &S_enPassant, &S_castling, &S_promotion);
    if (currentGame->playerKingInCheck())
    {
        if (currentGame->isCheckMate())
        {
            if (currentGame->getCurrentTurn() == Chess::WHITE_PLAYER)
            {
                cout << "Checkmate. Black wins" << endl;
            }
            else
            {
                cout << "Checkmate. White wins" << endl;
            }
        }
        else
        {
            if (currentGame->getCurrentTurn() == Chess::WHITE_PLAYER)
            {
                cout << "White is in check" << endl;
            }
            else
            {
                cout << "Black is in check" << endl;
            }
        }
    }
    return;
}

void gameTxt::run()
{
    bool bRun = true;
    clearScreen();
    string input = "";
    while(bRun)
    {
        printMenu();
        cout << "Type here: ";
        getline(cin,input);
        if (input.length() !=1)
        {
            cout << "Invalid input, only one letter pls" << endl;
            continue;
        }
        switch(input[0])
            {
                case 'N':
                case 'n':
                {
                    newGame();
                    clearScreen();
                    printSituation(*currentGame);
                    printBoard(*currentGame);
                    break;
                }

                case 'M':
                case 'm':
                {
                    if (currentGame != NULL)
                    {
                        if (currentGame->isFinished())
                        {
                            cout << "Game is over" << endl;
                        }
                        else
                        {
                            movePiece();
                            //clearScreen();
                            printSituation(*currentGame);
                            printBoard(*currentGame);
                        }
                    }
                    else cout <<"No game is being played" << endl;
                    break;
                }

                case 'Q':
                case 'q':
                {
                    bRun = false;
                    break;
                }
                default:
                {
                    cout << "Invalid command" << endl;
                }
            }
        }
}
