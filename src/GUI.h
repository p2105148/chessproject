#ifndef _GUI_H_
#define _GUI_H_

#include <SDL2/SDL.h>

class GUI {
public:
    GUI();
    ~GUI();

    void init();
    void createWindow();
    void createRenderer();
    void loadImages();
    void drawBoard();
    void handleMouseClick();
    void cleanup();

private:
    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;
    SDL_Texture* chessPiecesTexture[12];
    SDL_Texture* chessBoardTexture[2];
    SDL_Surface* chessPiecesSurface[12];
    SDL_Surface* chessBoardSurface[2];
};

#endif
