#include "GUI.h"
#include <iostream>
#include <SDL2/SDL_image.h>
using namespace std; 

GUI::GUI()
{
    window = nullptr;
    renderer = nullptr;
    for (int i = 0; i < 12; ++i) {
        chessPiecesTexture[i] = nullptr;
        chessPiecesSurface[i] = nullptr;
    }
    for (int i = 0; i < 2; ++i) {
        chessBoardTexture[i] = nullptr;
        chessBoardSurface[i] = nullptr;
    }
}

GUI::~GUI()
{
    cleanup();
}

void GUI::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        cout << "SDL_Init Error: " << SDL_GetError() << endl;
        exit(1);
    }
    createWindow();
    createRenderer();
    loadImages();
}

void GUI::createWindow()
{
    window = SDL_CreateWindow("Chess", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 800, SDL_WINDOW_SHOWN);
    if (window == nullptr)
    {
        cout << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
        cleanup();
        exit(1);
    }
}

void GUI::createRenderer()
{
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr)
    {
        cout << "SDL_CreateRenderer Error: " << SDL_GetError() << endl;
        cleanup();
        exit(1);
    }
}

void GUI::loadImages()
{
    chessPiecesSurface[0] = IMG_Load("data/white_pawn.png");
    chessPiecesSurface[1] = IMG_Load("data/white_rook.png");
    chessPiecesSurface[2] = IMG_Load("data/white_knight.png");
    chessPiecesSurface[3] = IMG_Load("data/white_bishop.png");
    chessPiecesSurface[4] = IMG_Load("data/white_queen.png");
    chessPiecesSurface[5] = IMG_Load("data/white_king.png");
    chessPiecesSurface[6] = IMG_Load("data/black_pawn.png");
    chessPiecesSurface[7] = IMG_Load("data/black_rook.png");
    chessPiecesSurface[8] = IMG_Load("data/black_knight.png");
    chessPiecesSurface[9] = IMG_Load("data/black_bishop.png");
    chessPiecesSurface[10] = IMG_Load("data/black_queen.png");
    chessPiecesSurface[11] = IMG_Load("data/black_king.png");

    chessPiecesTexture[0] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[0]);
    chessPiecesTexture[1] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[1]);
    chessPiecesTexture[2] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[2]);
    chessPiecesTexture[3] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[3]);
    chessPiecesTexture[4] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[4]);
    chessPiecesTexture[5] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[5]);
    chessPiecesTexture[6] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[6]);
    chessPiecesTexture[7] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[7]);
    chessPiecesTexture[8] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[8]);
    chessPiecesTexture[9] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[9]);
    chessPiecesTexture[10] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[10]);
    chessPiecesTexture[11] = SDL_CreateTextureFromSurface(renderer, chessPiecesSurface[11]);


    chessBoardSurface[0] = IMG_Load("data/light_square.png");
    chessBoardSurface[1] = IMG_Load("data/dark_square.png");

    chessBoardTexture[0] = SDL_CreateTextureFromSurface(renderer, chessBoardSurface[0]);
    chessBoardTexture[1] = SDL_CreateTextureFromSurface(renderer, chessBoardSurface[1]);
}