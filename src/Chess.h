#ifndef _CHESS_H_
#define _CHESS_H_

#include "includes.h"

/**
 * @brief La classe Chess répresente un game.
 */
class Chess 
{
    public:
    /**
     * @brief Prendre le couleur d'une piece.
     * 
     * @param chPiece Le charactère de la piece.
     * @return int le couleur pour la piece (0 pour blanc, 1 pour noir).
     */
    static int getPieceColor( char chPiece );

    /**
     * @brief Verifier si une piece est blanc.
     * 
     * @param chPiece La representation de la piece.
     * @return bool True si la piece est blanc, False sinon.
     */
    static bool isWhitePiece( char chPiece );

    /**
     * @brief Verifier si une piece est noir.
     * 
     * @param chPiece La representation de la piece.
     * @return bool True si la piece est noir, false sinon.
     */
    static bool isBlackPiece( char chPiece );

    /**
     * @brief Description d'une piece.
     * 
     * @param chPiece la representation de la piece.
     * @return std::string la description de la piece.
     */
    static string describePiece( char chPiece );

    //void testRegression();

    /**
     * @brief Enumeration Les couleurs des pieces.
     * 
     */
    enum PieceColor
    {
        WHITE_PIECE = 0,
        BLACK_PIECE = 1
    };

    /**
     * @brief  Enumeration Les joueurs.
     * 
     */
    enum Player
    {
        WHITE_PLAYER = 0,
        BLACK_PLAYER = 1
    };

    /**
     * @brief Enumeration Les directions.
     * 
     */
    enum Side
    {
        QUEEN_SIDE = 2,
        KING_SIDE  = 3
    };

    /**
     * @brief Enumeration Les directions.
     * 
     */
    enum Direction
    {
        HORIZONTAL = 0,
        VERTICAL,
        DIAGONAL,
        L_SHAPE
    };

    /**
     * @brief Structure Position.
     * 
     */

    struct Position
    {
        int iRow;
        int iColumn;
    };

    /**
     * @brief Structure EnPassant.
     * 
     */

    struct EnPassant
    {
        bool bApplied;
        Position PawnCaptured;
    };

    /**
     * @brief Structure Castling.
     * 
     */
    struct Castling
    {
        bool bApplied;
        Position rook_before;
        Position rook_after;
    };

    /**
     * @brief Structure Promotion.
     * 
     */
    struct Promotion
    {
        bool bApplied;
        char chBefore;
        char chAfter;
    };

    /**
     * @brief Structure IntendedMove.
     * 
     */
    struct IntendedMove
    {
        char chPiece;
        Position from;
        Position to;
    };

    /**
     * @brief Structure Attacker.
     * 
     */
    struct Attacker
    {
        Position  pos;
        Direction dir;
    };

    /**
     * @brief Structure UnderAttack.
     * 
     */
    struct UnderAttack
    {
        bool bUnderAttack;
        int iNumAttackers;
        Attacker attacker[9]; //maximum theoretical number of attackers
    };
};

#endif
