#ifndef _USER_INTERFACE_H_
#define _USER_INTERFACE_H_
#include "Chess.h"
#include "Game.h"

#define EMPTY_SQUARE 0x20

/**
 * @brief Effacer l'ecran.
 */
void clearScreen( void );

/**
 * @brief Afficher le menu.
 */
void printMenu( void );
/**
 * @brief Afficher la situation du jeu.
 * 
 * @param game 
 */
void printSituation(Game& game);

/**
 * @brief Afficher le tableau.
 * 
 * @param game 
 */
void printBoard(Game& game);

#endif