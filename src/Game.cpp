#include "Game.h"
#include "user_interface.h"
#include "includes.h"

Game::Game()
{
    m_CurrentTurn = WHITE_PLAYER;
    m_bGameFinished = false;
    m_bCastlingKingSideAllowed[WHITE_PLAYER] = true;
    m_bCastlingQueenSideAllowed[WHITE_PLAYER] = true;
    m_bCastlingKingSideAllowed[BLACK_PLAYER] = true;
    m_bCastlingQueenSideAllowed[BLACK_PLAYER] = true;
    board[0][0] = 'R';
    board[0][1] = 'N';
    board[0][2] = 'B';
    board[0][3] = 'Q';
    board[0][4] = 'K';
    board[0][5] = 'B';
    board[0][6] = 'N';
    board[0][7] = 'R';
    board[1][0] = 'P';
    board[1][1] = 'P';
    board[1][2] = 'P';
    board[1][3] = 'P';
    board[1][4] = 'P';
    board[1][5] = 'P';
    board[1][6] = 'P';
    board[1][7] = 'P';
    for (int i = 2; i <= 5; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            board[i][j] = ' ';
        }
    }
    board[6][0] = 'p';
    board[6][1] = 'p';
    board[6][2] = 'p';
    board[6][3] = 'p';
    board[6][4] = 'p';
    board[6][5] = 'p';
    board[6][6] = 'p';
    board[6][7] = 'p';
    board[7][0] = 'r';
    board[7][1] = 'n';
    board[7][2] = 'b';
    board[7][3] = 'q';
    board[7][4] = 'k';
    board[7][5] = 'b';
    board[7][6] = 'n';
    board[7][7] = 'r';

}

Game::~Game()
{
    white_captured.clear();
    black_captured.clear();
    rounds.clear();
}

void Game::movePiece( Position present, Position future, EnPassant* S_enPassant, Castling* S_castling, Promotion* S_promotion )
{
    char chPiece = getPieceAtPosition(present);
    char chCapturedPiece = getPieceAtPosition(future);

    if (' ' != chCapturedPiece)
    {
        if (getPieceColor(chCapturedPiece) == WHITE_PIECE )
        {
            white_captured.push_back(chCapturedPiece);
        }
        else
        {
            black_captured.push_back(chCapturedPiece);
        }
    }
    else if (S_enPassant -> bApplied == true)
    {
        char chCapturedEP = getPieceAtPosition( S_enPassant -> PawnCaptured.iRow, S_enPassant -> PawnCaptured.iColumn);
        if (getPieceColor(chCapturedEP) == WHITE_PIECE )
        {
            white_captured.push_back(chCapturedEP);
        }
        else
        {
            black_captured.push_back(chCapturedEP);
        }

        board[S_enPassant -> PawnCaptured.iRow][S_enPassant -> PawnCaptured.iColumn] = ' ';
    }
    board[present.iRow][present.iColumn] = ' ';

    if(S_promotion -> bApplied == true)
    {
        board[future.iRow][future.iColumn] = S_promotion -> chAfter;
    }
    else
    {
        board[future.iRow][future.iColumn] = chPiece;
    }

    if (S_castling -> bApplied == true)
    {
        char chPiece = getPieceAtPosition( S_castling -> rook_before );
        board[S_castling -> rook_before.iRow][S_castling -> rook_before.iColumn] = ' ';
        board[S_castling -> rook_after.iRow][S_castling -> rook_after.iColumn] = chPiece;
    }

    if(toupper(chPiece) == 'K')
    {
        m_bCastlingKingSideAllowed[getCurrentTurn()] = false;
        m_bCastlingQueenSideAllowed[getCurrentTurn()] = false;
    }
    else if (toupper(chPiece) == 'R')
    {
        if (present.iColumn == 0)
        {
            m_bCastlingQueenSideAllowed[getCurrentTurn()] = false;
        }
        else if (present.iColumn == 7)
        {
            m_bCastlingKingSideAllowed[getCurrentTurn()] = false;
        }
    }
    changeTurns();
}

bool Game::castlingAllowed( Side iSide, int iColor ) 
{
    if (iSide == KING_SIDE)
    {
        return m_bCastlingKingSideAllowed[iColor];
    }
    else
    {
        return m_bCastlingQueenSideAllowed[iColor];
    }
}

char Game::getPieceAtPosition(Position pos) 
{
    return board[pos.iRow][pos.iColumn];
}

char Game::getPieceAtPosition(int iRow, int iColumn) 
{
    return board[iRow][iColumn];
}

char Game::getPiece_considerMove(int iRow, int iColumn, IntendedMove* intended_move)
{
    char chPiece;
    if (intended_move == nullptr)
    {
        chPiece = getPieceAtPosition(iRow, iColumn);
    }
    else
    {
        if(intended_move -> from.iRow == iRow && intended_move -> from.iColumn == iColumn)
        {
            chPiece = ' ';
        }
        else if (intended_move->to.iRow == iRow && intended_move->to.iColumn == iColumn)
        {
            chPiece = intended_move->chPiece;
        }
        else
        {
            chPiece = getPieceAtPosition(iRow, iColumn);
        }
    }
    return chPiece;
}

Chess::UnderAttack Game::isUnderAttack(int iRow, int iColumn, int iColor, IntendedMove* pintended_move)
{
    UnderAttack attack  = {0};
    // Direction: Horizontal
    {
        //Right way
        for (int i = iColumn + 1;i<8;i++)
        {
            char chPieceFound = getPiece_considerMove(iRow, i, pintended_move);
            if (' ' == chPieceFound)
            {
                continue;
            }

            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = iRow;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = i;
                attack.attacker[attack.iNumAttackers-1].dir = HORIZONTAL;
                break;
            }
            else
            {
                break;
            }
        }
        //Left way
        for (int i = iColumn - 1;i>=0;i--)
        {
            char chPieceFound = getPiece_considerMove(iRow, i, pintended_move);
            if (' ' == chPieceFound)
            {
                continue;
            }

            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = iRow;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = i;
                attack.attacker[attack.iNumAttackers-1].dir = HORIZONTAL;
                break;
            }
            else
            {
                break;
            }
        }
    }
    // Vertical
    {
        //Way up
        for (int i= iRow +1; i<8; i++)
        {
            char chPieceFound = getPiece_considerMove(i, iColumn, pintended_move);
            if (chPieceFound == ' ')
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = iColumn;
                attack.attacker[attack.iNumAttackers-1].dir = VERTICAL;
                break;
            }
            else
            {
                break;
            }
        }
        //Way down
        for (int i=iRow - 1;i>=0;i--)
        {
            char chPieceFound = getPiece_considerMove(i, iColumn, pintended_move);
            if (chPieceFound == ' ')
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = iColumn;
                attack.attacker[attack.iNumAttackers-1].dir = VERTICAL;
                break;
            }
            else
            {
                break;
            }   
        }
    }
    // Diagonal
    {
        // up-right
        for (int i = iRow + 1, j = iColumn + 1 ; i<8 && j<8; i++, j++)
        {
            char chPieceFound = getPiece_considerMove(i, j, pintended_move);
            if (chPieceFound == ' ')
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if ((toupper(chPieceFound) == 'P') && (i == iRow + 1) && (j == iColumn + 1) && (iColor == WHITE_PIECE))
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                break;
            }
            else
            {
                break;
            }
        }
        // up-left
        {
            for (int i = iRow + 1, j = iColumn -1 ; i<8 && j>0; i++, j--)
            {
                char chPieceFound = getPiece_considerMove(i, j, pintended_move);
                if (chPieceFound == ' ')
                {
                    continue;
                }
                if (getPieceColor(chPieceFound) == iColor)
                {
                    break;
                }
                else if ((toupper(chPieceFound) == 'P') && (i == iRow + 1) && (j == iColumn - 1) && (iColor == WHITE_PIECE))
                {
                    attack.bUnderAttack = true;
                    attack.iNumAttackers++;

                    attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                    attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                    attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                    break;
                }
                else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
                {
                    attack.bUnderAttack = true;
                    attack.iNumAttackers++;

                    attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                    attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                    attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        // down-right
        {
            for (int i = iRow - 1, j = iColumn + 1; i > 0 && j < 8; i--, j++)
            {
                char chPieceFound = getPiece_considerMove(i, j, pintended_move);
                if (chPieceFound == ' ')
                {
                    continue;
                }
                if(getPieceColor(chPieceFound) == iColor)
                {
                    break;
                }
                else if ((toupper(chPieceFound) == 'P') && (i == iRow - 1) && (j == iColumn + 1) && (iColor == BLACK_PIECE))
                {
                    attack.bUnderAttack = true;
                    attack.iNumAttackers++;

                    attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                    attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                    attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                    break;
                }
                else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
                {
                    attack.bUnderAttack = true;
                    attack.iNumAttackers++;

                    attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                    attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                    attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        // down-left
        {
            for (int i = iRow - 1, j = iColumn - 1; i > 0 && j > 0; i--, j--)
            {
                char chPieceFound = getPiece_considerMove(i, j, pintended_move);
                if (chPieceFound == ' ')
                {
                    continue;
                }
                if(getPieceColor(chPieceFound) == iColor)
                {
                    break;
                }
                else if ((toupper(chPieceFound) == 'P') && (i == iRow - 1) && (j == iColumn - 1) && (iColor == BLACK_PIECE))
                {
                    attack.bUnderAttack = true;
                    attack.iNumAttackers++;

                    attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                    attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                    attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                    break;
                }
                else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
                {
                    attack.bUnderAttack = true;
                    attack.iNumAttackers++;

                    attack.attacker[attack.iNumAttackers-1].pos.iRow = i;
                    attack.attacker[attack.iNumAttackers-1].pos.iColumn = j;
                    attack.attacker[attack.iNumAttackers-1].dir = DIAGONAL;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
    }
    // L_Shaped
    {
        Position knight_moves[8] = { {1,-2}, {2, -1}, {2, 1}, {1, 2},
                                    {-1, -2}, {-2, -1}, {-2, 1}, {-1, 2} };
        for (int i=0;i<8;i++)
        {
            int iRowToTest = iRow + knight_moves[i].iRow;
            int iColumnToTest = iColumn + knight_moves[i].iColumn;

            if (iRowToTest < 0 || iRowToTest > 7 || iColumnToTest < 0 || iColumnToTest > 7)
            {
                // not exists
                continue;
            }

            char chPieceFound = getPiece_considerMove(iRowToTest, iColumnToTest, pintended_move);
            if (chPieceFound == ' ')
            {
                // empty
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                // same color
                continue;
            }
            else if ((toupper(chPieceFound) == 'N'))
            {
                attack.bUnderAttack = true;
                attack.iNumAttackers++;

                attack.attacker[attack.iNumAttackers-1].pos.iRow = iRowToTest;
                attack.attacker[attack.iNumAttackers-1].pos.iColumn = iColumnToTest;
                attack.attacker[attack.iNumAttackers-1].dir = L_SHAPE;
                break;
            }
        }
    }
    return attack;
}
bool Game::isReachable(int iRow, int iColumn, int iColor)
{
    bool bReachable = false;
    // Horizontal
    {
        // Right way
        for (int i = iColumn + 1; i<8; i++)
        {
            char chPieceFound = getPieceAtPosition(iRow, i);
            if (' ' == chPieceFound)
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
        // Left way
        for (int i= iColumn - 1 ; i>=0;i--)
        {
            char chPieceFound = getPieceAtPosition(iRow, i);
            if (' ' == chPieceFound)
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
    }
    // Vertical
    {
        //Way up
        for (int i= iRow+1; i<8; i++)
        {
            char chPieceFound = getPieceAtPosition(i, iColumn);
            if (chPieceFound == ' ')
            {
                continue;
            }
            if(getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'P' && getPieceColor(chPieceFound) == BLACK_PIECE && (i == iRow +1))
            {
                bReachable = true;
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
        // Way down
        for (int i= iRow - 1 ; i>= 0; i--)
        {
            char chPieceFound = getPieceAtPosition(i, iColumn);
            if (chPieceFound == ' ')
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'P' && getPieceColor(chPieceFound) == WHITE_PIECE && (i == iRow - 1))
            {
                bReachable = true;
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'R')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
    }
    // Diagonal
    {
        // up-right
        for (int i = iRow+1, j = iColumn + 1; i < 8 && j < 8;i++, j++)
        {
            char chPieceFound = getPieceAtPosition(i, j);
            if (chPieceFound == ' ')
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
        // up-left
        for (int i = iRow + 1, j = iColumn - 1; i < 8 && j > 0; i++, j--)
        {
            char chPieceFound = getPieceAtPosition(i, j);
            if (chPieceFound == EMPTY_SQUARE)
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
        // down-right
        for(int i = iRow - 1, j = iColumn + 1; i > 0 && j < 8; i--, j++)
        {
            char chPieceFound = getPieceAtPosition(i, j);
            if (chPieceFound == EMPTY_SQUARE)
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
        // down-left
        for (int i = iRow - 1, j = iColumn - 1; i > 0 && j > 0; i--, j--)
        {
            char chPieceFound = getPieceAtPosition(i, j);
            if (chPieceFound == EMPTY_SQUARE)
            {
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                break;
            }
            else if (toupper(chPieceFound) == 'Q' || toupper(chPieceFound) == 'B')
            {
                bReachable = true;
                break;
            }
            else
            {
                break;
            }
        }
    }
    // L_SHAPED
    {
        Position knight_moves[8] = { {1,-2}, {2, -1}, {2, 1}, {1, 2},
                                    {-1, -2}, {-2, -1}, {-2, 1}, {-1, 2} };
        for (int i=0;i<8;i++)
        {
            int iRowToTest = iRow + knight_moves[i].iRow;
            int iColumnToTest = iColumn + knight_moves[i].iColumn;

            if (iRowToTest < 0 || iRowToTest > 7 || iColumnToTest < 0 || iColumnToTest > 7)
            {
                // not exists
                continue;
            }

            char chPieceFound = getPieceAtPosition(iRowToTest, iColumnToTest);
            if (chPieceFound == EMPTY_SQUARE)
            {
                // empty
                continue;
            }
            if (getPieceColor(chPieceFound) == iColor)
            {
                // same color
                continue;
            }
            else if ((toupper(chPieceFound) == 'N'))
            {
                bReachable = true;
                break;
            }
        }
    }
    return bReachable;
}

bool Game::isSquareOccupied(int iRow, int iColumn)
{
    bool bOccupied = false;
    if (0x20 != getPieceAtPosition(iRow, iColumn))
    {
        bOccupied = true;
    }
    return bOccupied;
}

bool Game::isPathFree(Position startingPos, Position finishingPos, int iDirection)
{
    bool bFree = false;
    
    switch(iDirection)
    {
            case Chess::HORIZONTAL:
            {
                // If it's a horizontal move, we assume that the startingPos.iRow == finishingPos.iRow
                // If the piece wants to mive from column 0 to column 7, we must check if columns 1-6 are free
                if (startingPos.iColumn == finishingPos.iColumn)
                {
                    cout << "Error: Movement is horizontal but column is the same " << endl;
                }

                // moving to the right
                else if (startingPos.iColumn < finishingPos.iColumn)
                {
                    // setting bFree as initially true, only inside the cases, gurantees that the path is checked
                    bFree = true;

                    for (int i = startingPos.iColumn + 1; i < finishingPos.iColumn; i++)
                    {
                        if (isSquareOccupied( startingPos.iRow, i))
                        {
                            bFree = false;
                            cout << "Horizontal path to the right is not clear" << endl;
                        }
                    }
                }
                // moving to the left
                else // if (startingPos.iColumn > finishingPos.iColumn)
                {
                    bFree = true;
                    for (int i = startingPos.iColumn - 1; i > finishingPos.iColumn; i--)
                    {
                        if (isSquareOccupied( startingPos.iRow, i))
                        {
                            bFree = false;
                            cout << "Horizontal path to the left is not clear" << endl;
                        }
                    }
                }
            }
            break;

            case Chess::VERTICAL:
            {
                // If it's a vertical move, we assume that the startingPos.iColumn == finishingPos.iColumn
                // If the piece wants to move from row 0 to row 7, we must check if rows 1-6 are free
                if (startingPos.iRow == finishingPos.iRow)
                {
                    cout << "Error: Movement is vertical but row is the same " << endl;
                }

                // moving up
                else if (startingPos.iRow < finishingPos.iRow)
                {
                    bFree = true;
                    for (int i = startingPos.iRow + 1; i < finishingPos.iRow; i++)
                    {
                        if (isSquareOccupied( i, startingPos.iColumn))
                        {
                            bFree = false;
                            cout << "Vertical path up is not clear" << endl;
                        }
                    }
                }
                // moving down
                else // if (startingPos.iRow > finishingPos.iRow)
                {
                    bFree = true;
                    for (int i = startingPos.iRow - 1; i > finishingPos.iRow; i--)
                    {
                        if (isSquareOccupied( i, startingPos.iColumn))
                        {
                            bFree = false;
                            cout << "Vertical path down is not clear" << endl;
                        }
                    }
                }
            }
            break;

            case Chess::DIAGONAL:
            {
                //Moving up and right
                if ((finishingPos.iRow > startingPos.iRow) && (finishingPos.iColumn > startingPos.iColumn))
                {
                    bFree = true;
                    for (int i = 1;i < abs(finishingPos.iRow - startingPos.iRow); i++)
                    {
                        if (isSquareOccupied( startingPos.iRow + i, startingPos.iColumn + i))
                        {
                            bFree = false;
                            cout << "Diagonal path up and right is not clear" << endl;
                        }
                    }
                }
                // moving up and left
                else if ((finishingPos.iRow > startingPos.iRow) && (finishingPos.iColumn < startingPos.iColumn))
                {
                    bFree = true;
                    for (int i = 1;i < abs(finishingPos.iRow - startingPos.iRow); i++)
                    {
                        if (isSquareOccupied( startingPos.iRow + i, startingPos.iColumn - i))
                        {
                            bFree = false;
                            cout << "Diagonal path up and left is not clear" << endl;
                        }
                    }
                }
                // moving down and right
                else if ((finishingPos.iRow < startingPos.iRow) && (finishingPos.iColumn > startingPos.iColumn))
                {
                    bFree = true;
                    for (int i = 1;i < abs(finishingPos.iRow - startingPos.iRow); i++)
                    {
                        if (isSquareOccupied( startingPos.iRow - i, startingPos.iColumn + i))
                        {
                            bFree = false;
                            cout << "Diagonal path down and right is not clear" << endl;
                        }
                    }
                }
                // moving down and left
                else if ((finishingPos.iRow < startingPos.iRow) && (finishingPos.iColumn < startingPos.iColumn))
                {
                    bFree = true;
                    for (int i = 1;i < abs(finishingPos.iRow - startingPos.iRow); i++)
                    {
                        if (isSquareOccupied( startingPos.iRow - i, startingPos.iColumn - i))
                        {
                            bFree = false;
                            cout << "Diagonal path down and left is not clear" << endl;
                        }
                    }
                }   
                else
                {
                    cout << "Error: Invalid diagonal movement" << endl;
                }
            }
            break;
    }  
    return bFree;
}

bool Game::canBeBlocked(Position startingPos, Position finishingPos, int iDirection)
{
    bool bBlocked = false;
    //Chess::UnderAttack blocker = {0};
    switch(iDirection)
    {
        case Chess::HORIZONTAL:
        {
            if(startingPos.iColumn == finishingPos.iColumn)
            {
                cout << "Error: Movement is horizontal but column is the same" << endl;
            }
            // moving to the right
            else if (startingPos.iColumn < finishingPos.iColumn)
            {
                for (int i = startingPos.iColumn + 1; i < finishingPos.iColumn; i++)
                {
                    if (isReachable(startingPos.iRow, i, getOpponentColor()))
                    {
                       bBlocked = true;
                    }
                }
            }
            // moving to the left
            else // if (startingPos.iColumn > finishingPos.iColumn)
            {
                for (int i = startingPos.iColumn - 1; i > finishingPos.iColumn; i--)
                {
                    if (isReachable(startingPos.iRow, i, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
        }
        break;

        case Chess::VERTICAL:
        {
            if (startingPos.iRow == finishingPos.iRow)
            {
                cout << "Error: Movement is vertical but row is the same" << endl;
            }
            // moving up
            else if (startingPos.iRow < finishingPos.iRow)
            {
                for (int i = startingPos.iRow + 1 ; i<finishingPos.iRow; i++)
                {
                    if (isReachable(i, startingPos.iColumn, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
            // moving down
            else // if (startingPos.iRow > finishingPos.iRow)
            {
                for (int i = startingPos.iRow - 1; i > finishingPos.iRow; i--)
                {
                    if (isReachable(i, startingPos.iColumn, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
        }
        break;

        case Chess::DIAGONAL:
        {
            // up and right
            if ((finishingPos.iRow > startingPos.iRow) && (finishingPos.iColumn > startingPos.iColumn))
            {
                for (int i = 1; i < abs(finishingPos.iRow - startingPos.iRow); i++)
                {
                    if (isReachable(startingPos.iRow + i, startingPos.iColumn + i, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
            // up and left
            else if ((finishingPos.iRow > startingPos.iRow) && (finishingPos.iColumn < startingPos.iColumn))
            {
                for (int i = 1; i < abs(finishingPos.iRow - startingPos.iRow); i++)
                {
                    if (isReachable(startingPos.iRow + i, startingPos.iColumn - i, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
            // down and right
            else if ((finishingPos.iRow < startingPos.iRow) && (finishingPos.iColumn > startingPos.iColumn))
            {
                for (int i = 1; i < abs(finishingPos.iRow - startingPos.iRow); i++)
                {
                    if (isReachable(startingPos.iRow - i, startingPos.iColumn + i, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
            // down and left
            else if ((finishingPos.iRow < startingPos.iRow) && (finishingPos.iColumn < startingPos.iColumn))
            {
                for (int i = 1; i < abs(finishingPos.iRow - startingPos.iRow); i++)
                {
                    if (isReachable(startingPos.iRow - i, startingPos.iColumn - i, getOpponentColor()))
                    {
                        bBlocked = true;
                    }
                }
            }
            else
            {
                cout << "Error: Invalid diagonal movement" << endl;
            }
        }
    }
    return bBlocked;
}

bool Game::isCheckMate()
{
    bool bCheckMate = false;
    // 1, is the king in check?
    if (playerKingInCheck() == false)
    {
        bCheckMate = false;
    }
    // 2, Can the king move to another square?
    Chess::Position king_moves[8] = { {1,-1}, {1,0}, {1,1}, {0,1},
                                    {-1,1}, {-1,0}, {-1,-1}, {0,-1} };
    
    Chess::Position king = findKing(getCurrentTurn());
    for (int i=0;i<8;i++)
    {
        int iRowToTest = king.iRow + king_moves[i].iRow;
        int iColumnToTest = king.iColumn + king_moves[i].iColumn;

        if (iRowToTest < 0 || iRowToTest > 7 || iColumnToTest < 0 || iColumnToTest > 7)
        {
            continue;
        }

        if (getPieceAtPosition(iRowToTest, iColumnToTest) != EMPTY_SQUARE)
        {
            continue;
        }

        Chess::IntendedMove intended_move;
        intended_move.chPiece = getPieceAtPosition(king.iRow, king.iColumn);
        intended_move.from.iRow = king.iRow;
        intended_move.from.iColumn = king.iColumn;
        intended_move.to.iRow = iRowToTest;
        intended_move.to.iColumn = iColumnToTest;

        Chess::UnderAttack king_moved = isUnderAttack(iRowToTest, iColumnToTest, getCurrentTurn(), &intended_move);
        if (king_moved.bUnderAttack == false)
        {
            // this means there's at least one pos when the king move would not be under attack
            return false;
        }
    }
    // 3, Can the attacking piece be captured? Or be blocked?
    Chess::UnderAttack king_attacked = isUnderAttack(king.iRow, king.iColumn, getCurrentTurn());
    if (king_attacked.iNumAttackers == 1)
    {
        // Can the attacking piece be captured?
        Chess::UnderAttack king_attacker = {0};
        king_attacker = isUnderAttack(king_attacked.attacker[0].pos.iRow, king_attacked.attacker[0].pos.iColumn, getOpponentColor());
        if (king_attacker.bUnderAttack == true)
        {
            // this means the attacking piece can be captured, so it's not a checkmate
            return false;
        }
        else
        {
            // Can the attacking piece be blocked?
            char chAttacker = getPieceAtPosition(king_attacked.attacker[0].pos);
            switch(toupper(chAttacker))
            {
                // if it's a pawn, there's no space in between the king and the pawn
                case 'P':
                // if it's a knight, it can't be blocked, this is checkmate
                case 'N':
                {
                    bCheckMate = true;
                }
                break;

                case 'B':
                {
                    if (canBeBlocked(king_attacked.attacker[0].pos, king, Chess::DIAGONAL) == false)
                    {
                        bCheckMate = true;
                    }
                }
                break;

                case 'R':
                {
                    if (canBeBlocked(king_attacked.attacker[0].pos, king, king_attacked.attacker[0].dir) == false)
                    {
                        bCheckMate = true;
                    }
                }
                break;

                case 'Q':
                {
                    if (canBeBlocked(king_attacked.attacker[0].pos, king, king_attacked.attacker[0].dir) == false)
                    {
                        bCheckMate = true;
                    }
                }
                break;

                default:
                {
                    cout << "Error: Invalid piece" << endl;
                }
                break;
            }
        }
    }
    else
    {
        bCheckMate = true;
    }
    m_bGameFinished = bCheckMate;
    return bCheckMate;
}

bool Game::isKingInCheck(int iColor, IntendedMove* pintended_move)
{
    bool bCheck = false;
    Position king = {0};
    //Must check if the intended move is to move the king itself
    if (pintended_move != nullptr && toupper(pintended_move -> chPiece) == 'K')
    {
        king.iRow = pintended_move -> to.iRow;
        king.iColumn = pintended_move -> to.iColumn;
    }
    else
    {
        king = findKing(iColor);
    }

    UnderAttack king_attacked = isUnderAttack(king.iRow, king.iColumn, iColor, pintended_move);
    if (king_attacked.bUnderAttack == true)
    {
        bCheck = true;
    }
    return bCheck;
}

bool Game::playerKingInCheck(IntendedMove* intended_move)
{
    return isKingInCheck(getCurrentTurn(), intended_move);
}

bool Game::wouldKingBeInCheck(char chPiece, Position present, Position future, EnPassant* S_enPassant)
{
    IntendedMove intended_move;
    
    intended_move.chPiece = chPiece;
    intended_move.from = present;
    intended_move.to = future;
    return playerKingInCheck(&intended_move);
}

Chess::Position Game::findKing(int iColor)
{
    char chToLook = (iColor == WHITE_PIECE) ? 'K' : 'k';
    Position king = {0};
    for (int i=0;i<8;i++)
    {
        for (int j=0;j<8;j++)
        {
            if (getPieceAtPosition(i,j) == chToLook)
            {
                king.iRow = i;
                king.iColumn = j;
            }
        }
    }
    return king;
}

void Game::changeTurns(void)
{
    if (m_CurrentTurn == WHITE_PIECE)
    {
        m_CurrentTurn = BLACK_PIECE;
    }
    else
    {
        m_CurrentTurn = WHITE_PIECE;
    }
}

bool Game::isFinished(void)
{
    return m_bGameFinished;
}

int Game::getCurrentTurn(void)
{
    return m_CurrentTurn;
}

int Game::getOpponentColor(void)
{
    return (m_CurrentTurn == WHITE_PIECE) ? BLACK_PIECE : WHITE_PIECE;
}

void Game::parseMove(string move, Position* pFrom, Position* pTo, char* chPromoted)
{
    pFrom->iColumn = move[0];
    pFrom->iRow = move[1];
    pTo->iColumn = move[3];
    pTo->iRow = move[4];

    pFrom ->iColumn -= 'A';
    pTo ->iColumn -= 'A';

    pFrom ->iRow -= '1';
    pTo ->iRow -= '1';

    if (chPromoted != nullptr)
    {
        if(move[5] == '=')
        {
            *chPromoted = move[6];
        }
        else
        {
            *chPromoted = EMPTY_SQUARE;
        }
    }
}

void Game::logMove(string &to_record)
{
    if (to_record.length() == 5)
    {
        to_record += " ";
    }
    if (getCurrentTurn() == WHITE_PIECE)
    {
        Round round;
        round.white_move = to_record;
        round.black_move = "";
    }
    else
    {
        Round round = rounds[rounds.size() - 1];
        round.black_move = to_record;

        rounds.pop_back();
        rounds.push_back(round);
    }
}

string Game::getLastMove()
{
   string last_move;

   // Who did the last move?
   if (BLACK_PLAYER == getCurrentTurn())
   {
      // If it's black's turn now, white had the last move
      last_move = rounds[rounds.size() - 1].white_move;
   }
   else
   {
      // Last move was black's
      last_move = rounds[rounds.size() - 1].black_move;
   }

   return last_move;
}

void Game::deleteLastMove()
{

    // Notice we already changed turns back
    if (WHITE_PLAYER == getCurrentTurn())
    {
        // Last move was white's turn, so simply pop from the back
        rounds.pop_back();
    }
    else
    {
        // Last move was black's, so let's 
        Round round = rounds[rounds.size() - 1];
        round.black_move = "";

        // Pop last round and put it back, now without the black move
        rounds.pop_back();
        rounds.push_back(round);
    }
}