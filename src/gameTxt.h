#ifndef _GAMETXT_H_
#define _GAMETXT_H_
#include "includes.h"
#include "user_interface.h"
#include "Game.h"

/**
 * @brief La classe gameTxt répresente un game en mode texte.
 */
class gameTxt : public Game
{
    public:
        /**
         * @brief Constructeur de la classe gameTxt.
         */
        gameTxt();
        /**
         * @brief Destructeur de la classe gameTxt.
         */
        ~gameTxt();
        /**
         * @brief Verifier si un mouvement est valide.
         * 
         * @param present La position actuelle de la piece.
         * @param future La position future de la piece.
         * @param S_enPassant Pointeur vers l'objet EnPassant.
         * @param S_castling Pointeur vers l'objet Castling.
         * @param S_promotion Pointeur vers l'objet Promotion.
         * @return bool True si le mouvement est valide, False sinon.
         */
        bool isMoveValid(Chess::Position present, Chess::Position future, Chess::EnPassant* S_enPassant, Chess::Castling* S_castling, Chess::Promotion* S_promotion);
        /**
         * @brief Faire un mouvement.
         * 
         * @param present La position actuelle de la piece.
         * @param future La position future de la piece.
         * @param S_enPassant Pointeur vers l'objet EnPassant.
         * @param S_castling Pointeur vers l'objet Castling.
         * @param S_promotion Pointeur vers l'objet Promotion.
         */
        void makeTheMove(Chess::Position present, Chess::Position future, Chess::EnPassant* S_enPassant, Chess::Castling* S_castling, Chess::Promotion* S_promotion);
        /**
         * @brief Nouveau jeu.
         */
        void newGame();
        /**
         * @brief faire un move
         * 
         */
        void movePiece();
        /**
         * @brief Executer le jeu.
         * 
         */
        void run();
    private:
        Game* currentGame;
};

#endif